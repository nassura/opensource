## FileName:          Makefile
## Description:       Please read the notes in detail, do not change
## Instructions:      only revise below step1 ~ step3
## history:           v1.0

# 定义的固定开源组件目录, 不要改
pwd                     := $(shell pwd)
pkg_build_root          := $(pwd)/build
pkg_patches_root        := $(pwd)/patches
pkg_storage_root        := $(pwd)/packages
pkg_script_root         := $(pwd)/script
pkg_control_root        := $(pwd)/control

pkg                     := mpc
pkg_version             := 1.2.1
pkg_license             := LGPL
pkg_summary             := mpc
pkg_desc                := mpc
pkg_download_url        := http//ftp.gnu.org/gnu/mpc
pkg_extra_config        := --host=$(CROSS) --prefix=/usr
pkg_extra_config        += --disable-static
pkg_extra_config        += CFLAGS="$(filter-out -Werror,$(CFLAGS))"
pkg_location            := $(pkg_storage_root)/$(pkg)-$(pkg_version).tar.gz

pkg_build_dir           := $(pkg_build_root)/$(pkg)-$(pkg_version)
pkg_patches_dir         := $(pkg_patches_root)/$(pkg)-$(pkg_version)
control                 := $(pkg_control_root)/control
package                 := package
version                 := version
license                 := license
download_url            := download_url

# 不要改, 开源产物最终临时存放目录
pkg_tmp_root            := $(pwd)/install
pkg_tmp_install_dir     := $(pkg_tmp_root)/$(pkg)-$(pkg_version)

# 不要改, 通用定义
pkg_info_name           := $(pkg_tmp_install_dir)/$(pkg).readme

# 最大任务并行数量
PARALLEL_JOBS := $(shell echo \
	$$((1 + `getconf _NPROCESSORS_ONLN 2>/dev/null || echo 1`)))

# 不要改, 通用接口
# 打补丁
# 参数说明
# $1: 源码的根目录
# $2: 存放补丁的目录
define patch_src
@if test -d "$1" && test -d "$2"; then \
    cd "$1"; \
    for pa in `find "$2" -type f -name "*.patch" | sort`; do \
        patch -p1 < $${pa}; \
    done; \
fi
endef

.PHONY: all
all: clean install

# 正常不用改, 解压安装包和打patch
.PHONY: source_dir
source_dir: $(pkg_location)
	@echo "tar $(pkg_location) to $(pkg_build_root)..."
	mkdir -p $(pkg_build_root)
	tar -xf $< -C $(pkg_build_root)
	test -d $(pkg_build_dir)
	$(call patch_src,$(pkg_build_dir),$(pkg_patches_dir))

# 正常不用改, 配置开源安装路径和编译安装
.PHONY: build
build: source_dir
	@echo "building $(pkg) ..."
	cd $(pkg_build_dir) && \
	./configure $(pkg_extra_config) && \
	$(MAKE) -j$(PARALLEL_JOBS) && \
	$(MAKE) install DESTDIR=$(pkg_tmp_install_dir) -j$(PARALLEL_JOBS)

$(pkg_info_name):
	@echo "wirte package info to readme ..."
	@echo "$(package): $(pkg)"                           > $@
	@echo "$(version): $(pkg_version)"                  >> $@
	@echo "$(license): $(pkg_license)"                  >> $@
	@echo "$(download_url): $(pkg_download_url)"        >> $@

$(control):
	@install -m0755 -d $(pkg_control_root)
	@echo "generate $(control)..."
	@echo "Package: $(pkg)"                     > $@
	@echo "Opensource_version: $(pkg_version)" >> $@
	@echo "Depends:"                           >> $@
	@echo "Is_reboot: 1"                       >> $@
	@echo "Description: $(pkg_desc)"           >> $@
	@echo "Source: $(pkg_download_url)"        >> $@

## 有新增或升级时, 这里将缓存目录拷贝到设备实际安装目录
.PHONY: install
install: build $(pkg_info_name) $(control)
	@echo "starting install $(pkg) ..."
	install -m0755 -d $(PKG_BUILD_PATH_ROOT)/opensource
	cp --remove-destination -pa $(pkg_info_name) $(PKG_BUILD_PATH_ROOT)/opensource
	install -m0755 -d $(PKG_BUILD_PATH_INCLUDE)
	install -m0755 -d $(PKG_BUILD_PATH_LIB)
	install -m0755 -d $(PKG_INSTALL_PATH_ROOTFS)/usr/lib
	cp --remove-destination -pa $(pkg_tmp_install_dir)/usr/include/. $(PKG_BUILD_PATH_INCLUDE)
	cp --remove-destination -pa $(pkg_tmp_install_dir)/usr/lib/lib*.so* $(PKG_BUILD_PATH_LIB)
	cp --remove-destination -pa $(pkg_tmp_install_dir)/usr/lib/lib*.so.* $(PKG_INSTALL_PATH_ROOTFS)/usr/lib

# 不要改, 清除编译前产物
.PHONY: clean
clean:
	rm -rf $(pkg_tmp_install_dir)
	rm -rf $(pkg_info_name)
	rm -rf $(pkg_build_dir)
	rm -rf $(control)
