## FileName:          Makefile
## Description:       Please read the notes in detail, do not change
## Instructions:      only revise below step1 ~ step3
## history:           v1.0

# 定义的固定开源组件目录, 不要改
pwd                     := $(shell pwd)
pkg_build_root          := $(pwd)/build
pkg_patches_root        := $(pwd)/patches
pkg_storage_root        := $(pwd)/packages
pkg_script_root         := $(pwd)/script
pkg_control_root        := $(pwd)/control

pkg                     := busybox
pkg_version             := 1.36.1
pkg_license             := GPLv2
pkg_summary             := Busybox
pkg_desc                := The Swiss Army Knife of Embedded Linux
pkg_download_url        := https://busybox.net/
pkg_extra_config        :=
pkg_location            := $(pkg_storage_root)/$(pkg)-$(pkg_version).tar.bz2

# 不要改, 通用定义
CPPFLAGS                += -Wno-format-security

# 不要改, 通用定义
pkg_build_dir           := $(pkg_build_root)/$(pkg)-$(pkg_version)
pkg_patches_dir         := $(pkg_patches_root)/$(pkg)-$(pkg_version)
control                 := $(pkg_control_root)/control
package                 := package
version                 := version
license                 := license
download_url            := download_url

# 不要改, 开源产物最终临时存放目录
pkg_tmp_root            := $(pwd)/install
pkg_tmp_install_dir     := $(pkg_tmp_root)/$(pkg)-$(pkg_version)

# 不要改, 通用定义
pkg_info_name           := $(pkg_tmp_install_dir)/$(pkg).readme

# 不要改, 通用接口
# 打补丁
# 参数说明
# $1: 源码的根目录
# $2: 存放补丁的目录
define patch_src
@if test -d "$1" && test -d "$2"; then \
    cd "$1"; \
    for pa in `find "$2" -type f -name "*.patch" | sort`; do \
        patch -p1 < $${pa}; \
    done; \
fi
endef

.PHONY: all
all: clean install

# 正常不用改, 解压安装包和打patch
.PHONY: source_dir
source_dir: $(pkg_location)
	@echo "tar $(pkg_location) to $(pkg_build_root)..."
	mkdir -p $(pkg_build_root)
	tar -xf $< -C $(pkg_build_root)
	test -d $(pkg_build_dir)
	$(call patch_src,$(pkg_build_dir),$(pkg_patches_dir))
	cp configs/$(pkg)-$(pkg_version).config $(pkg_build_dir)/.config

# 正常不用改, 配置开源安装路径和编译安装
.PHONY: build
build: source_dir
	@echo "building $(pkg) ..."
	unset CFLAGS CPPFLAGS LDFLAGS; $(MAKE) -C $(pkg_build_dir) -j$(MAX_JOBS) CROSS_COMPILE=$(CROSS_COMPILE) KBUILD_OUTPUT=
	unset CFLAGS CPPFLAGS LDFLAGS; $(MAKE) -C $(pkg_build_dir) -j$(MAX_JOBS) CROSS_COMPILE=$(CROSS_COMPILE) KBUILD_OUTPUT= busybox.links
	for f in sbin/init sbin/logread bin/ar bin/pidof bin/kill bin/watch bin/ps bin/mount bin/umount bin/lspci sbin/reboot sbin/sysctl; do sed -i "\?$${f}\$$?d" $(pkg_build_dir)/busybox.links; done
	unset CFLAGS CPPFLAGS LDFLAGS; $(MAKE) -C $(pkg_build_dir) -j$(MAX_JOBS) CROSS_COMPILE=$(CROSS_COMPILE) KBUILD_OUTPUT= install
	cp -pa --remove-destination $(pkg_build_dir)/busybox_unstripped $(pkg_build_dir)/_install/bin/busybox

$(pkg_info_name):
	@install -m0755 -d $(@D)
	@echo "wirte package info to readme ..."
	@echo "$(package): $(pkg)"                           > $@
	@echo "$(version): $(pkg_version)"                  >> $@
	@echo "$(license): $(pkg_license)"                  >> $@
	@echo "$(download_url): $(pkg_download_url)"        >> $@

$(control):
	@install -m0755 -d $(pkg_control_root)
	@echo "generate $(control)..."
	@echo "Package: $(pkg)"                     > $@
	@echo "Opensource_version: $(pkg_version)" >> $@
	@echo "Depends:"                           >> $@
	@echo "Is_reboot: 1"                       >> $@
	@echo "Description: $(pkg_desc)"           >> $@
	@echo "Source: $(pkg_download_url)"        >> $@

## 有新增或升级时, 这里将缓存目录拷贝到设备实际安装目录
.PHONY: install
install: build $(pkg_info_name) $(control)
	@echo "starting install $(pkg) ..."
	install -m0755 -d $(PKG_BUILD_PATH_ROOT)/opensource
	cp --remove-destination -pa $(pkg_info_name) $(PKG_BUILD_PATH_ROOT)/opensource
	cp -pa --remove-destination $(pkg_build_dir)/_install/. $(PKG_INSTALL_PATH_ROOTFS)
	cp -pa --remove-destination $(pkg_build_dir)/busybox_unstripped \
		$(PKG_INSTALL_PATH_ROOTFS)/bin/busybox

# 不要改, 清除编译前产物
.PHONY: clean
clean:
	rm -rf $(pkg_tmp_install_dir)
	rm -rf $(pkg_info_name)
	rm -rf $(pkg_build_dir)
	rm -rf $(control)
